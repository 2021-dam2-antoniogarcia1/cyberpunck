-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-12-2020 a las 16:53:13
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `persona`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `ID` int(11) NOT NULL,
  `Gremio` text NOT NULL,
  `Nombre` text NOT NULL,
  `Apellido` text NOT NULL,
  `Género` text NOT NULL,
  `Sexo` text NOT NULL,
  `Edad` int(11) NOT NULL,
  `Etnia` text NOT NULL,
  `Estado Civil` text NOT NULL,
  `Dieta` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`ID`, `Gremio`, `Nombre`, `Apellido`, `Género`, `Sexo`, `Edad`, `Etnia`, `Estado Civil`, `Dieta`) VALUES
(1, 'A', 'Antonio', 'Garcia', 'Helicóptero de combate', 'Masculino', 22, 'Caucásico', 'Soltero', 'Omnívoro'),
(2, 'B', 'Cacapo', 'Apaco', 'Pajaro', 'Femenino', 18, 'Pajaro', 'Pajareado', 'Insectívoro'),
(3, 'C', 'Pocholo', 'Montero', 'Femenino', 'Masculino', 34, 'Asiatico', 'Casado', 'Vegano'),
(4, 'D', 'Franchesco', 'Virgolini', 'Moto', 'Masculino', 45, 'Caucásico', 'Soltero', 'Omnívoro'),
(5, 'E', 'Haplo', 'Shafer', 'Vampiro', 'Por determinar', 31, 'Caucásico', 'Soltero', 'Carnívoro'),
(6, 'F', 'Aramis', 'Fuster', 'Domintrix', 'Femenino', 65, 'Caucásico', 'Soltera', 'Jóvenes '),
(7, 'B', 'Dalas', 'Campos', 'Masculino', 'Masculino', 36, 'Caucásico', 'Casado', 'Vegano'),
(8, 'A', 'Copito', 'Domingo', 'Femenino', 'Femenino', 25, 'Caucásico', 'Soltera', 'Vegano'),
(9, 'E', 'Estephano', 'Prados', 'Femenino', 'Masculino', 29, 'Caucásico', 'Soltero', 'Vegano'),
(10, 'C', 'Black', 'Rose', 'Femenino', 'Femenino', 35, 'Caucásico', 'Soltera', 'Omnívoro'),
(11, 'A', 'Unatxi', 'Kamala', 'Masculino', 'Femenino', 22, 'Caucásico', 'Soltero', 'Omnívoro'),
(12, 'B', 'Rosa', 'Melano', 'Femenino', 'Femenino', 78, 'Asiático', 'Casado', 'Vegano');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
