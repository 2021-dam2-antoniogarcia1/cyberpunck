﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form3
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Logo = New System.Windows.Forms.PictureBox()
        Me.IdViviendaDato = New System.Windows.Forms.TextBox()
        Me.Volver = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Cerrar = New System.Windows.Forms.Button()
        Me.PuertaDato = New System.Windows.Forms.TextBox()
        Me.PlantaDato = New System.Windows.Forms.TextBox()
        Me.EdificioDato = New System.Windows.Forms.TextBox()
        Me.SectorDato = New System.Windows.Forms.TextBox()
        Me.PuertaLabel = New System.Windows.Forms.Label()
        Me.PlantaLabel = New System.Windows.Forms.Label()
        Me.EdificioLabel = New System.Windows.Forms.Label()
        Me.SectorLabel = New System.Windows.Forms.Label()
        Me.IdViviendaLabel = New System.Windows.Forms.Label()
        CType(Me.Logo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Logo
        '
        Me.Logo.Image = Global.Cyberpunck.My.Resources.Resources.logo
        Me.Logo.Location = New System.Drawing.Point(346, 17)
        Me.Logo.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Logo.Name = "Logo"
        Me.Logo.Size = New System.Drawing.Size(120, 123)
        Me.Logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Logo.TabIndex = 69
        Me.Logo.TabStop = False
        '
        'IdViviendaDato
        '
        Me.IdViviendaDato.Location = New System.Drawing.Point(34, 64)
        Me.IdViviendaDato.Name = "IdViviendaDato"
        Me.IdViviendaDato.Size = New System.Drawing.Size(164, 26)
        Me.IdViviendaDato.TabIndex = 68
        '
        'Volver
        '
        Me.Volver.Location = New System.Drawing.Point(340, 228)
        Me.Volver.Name = "Volver"
        Me.Volver.Size = New System.Drawing.Size(114, 45)
        Me.Volver.TabIndex = 67
        Me.Volver.Text = "Volver"
        Me.Volver.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(340, 170)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(114, 45)
        Me.Button3.TabIndex = 66
        Me.Button3.Text = "Modificar"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Cerrar
        '
        Me.Cerrar.Location = New System.Drawing.Point(340, 290)
        Me.Cerrar.Name = "Cerrar"
        Me.Cerrar.Size = New System.Drawing.Size(114, 45)
        Me.Cerrar.TabIndex = 65
        Me.Cerrar.Text = "Cerrar"
        Me.Cerrar.UseVisualStyleBackColor = True
        '
        'PuertaDato
        '
        Me.PuertaDato.Location = New System.Drawing.Point(128, 297)
        Me.PuertaDato.Name = "PuertaDato"
        Me.PuertaDato.Size = New System.Drawing.Size(164, 26)
        Me.PuertaDato.TabIndex = 64
        '
        'PlantaDato
        '
        Me.PlantaDato.Location = New System.Drawing.Point(128, 239)
        Me.PlantaDato.Name = "PlantaDato"
        Me.PlantaDato.Size = New System.Drawing.Size(164, 26)
        Me.PlantaDato.TabIndex = 63
        '
        'EdificioDato
        '
        Me.EdificioDato.Location = New System.Drawing.Point(128, 177)
        Me.EdificioDato.Name = "EdificioDato"
        Me.EdificioDato.Size = New System.Drawing.Size(164, 26)
        Me.EdificioDato.TabIndex = 62
        '
        'SectorDato
        '
        Me.SectorDato.Location = New System.Drawing.Point(128, 119)
        Me.SectorDato.Name = "SectorDato"
        Me.SectorDato.Size = New System.Drawing.Size(164, 26)
        Me.SectorDato.TabIndex = 61
        '
        'PuertaLabel
        '
        Me.PuertaLabel.AutoSize = True
        Me.PuertaLabel.Location = New System.Drawing.Point(29, 302)
        Me.PuertaLabel.Name = "PuertaLabel"
        Me.PuertaLabel.Size = New System.Drawing.Size(56, 20)
        Me.PuertaLabel.TabIndex = 60
        Me.PuertaLabel.Text = "Puerta"
        '
        'PlantaLabel
        '
        Me.PlantaLabel.AutoSize = True
        Me.PlantaLabel.Location = New System.Drawing.Point(29, 244)
        Me.PlantaLabel.Name = "PlantaLabel"
        Me.PlantaLabel.Size = New System.Drawing.Size(54, 20)
        Me.PlantaLabel.TabIndex = 59
        Me.PlantaLabel.Text = "Planta"
        '
        'EdificioLabel
        '
        Me.EdificioLabel.AutoSize = True
        Me.EdificioLabel.Location = New System.Drawing.Point(29, 182)
        Me.EdificioLabel.Name = "EdificioLabel"
        Me.EdificioLabel.Size = New System.Drawing.Size(60, 20)
        Me.EdificioLabel.TabIndex = 58
        Me.EdificioLabel.Text = "Edificio"
        '
        'SectorLabel
        '
        Me.SectorLabel.AutoSize = True
        Me.SectorLabel.Location = New System.Drawing.Point(29, 124)
        Me.SectorLabel.Name = "SectorLabel"
        Me.SectorLabel.Size = New System.Drawing.Size(56, 20)
        Me.SectorLabel.TabIndex = 57
        Me.SectorLabel.Text = "Sector"
        '
        'IdViviendaLabel
        '
        Me.IdViviendaLabel.AutoSize = True
        Me.IdViviendaLabel.Location = New System.Drawing.Point(29, 41)
        Me.IdViviendaLabel.Name = "IdViviendaLabel"
        Me.IdViviendaLabel.Size = New System.Drawing.Size(90, 20)
        Me.IdViviendaLabel.TabIndex = 56
        Me.IdViviendaLabel.Text = "ID Vivienda"
        '
        'Form3
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.ClientSize = New System.Drawing.Size(494, 352)
        Me.Controls.Add(Me.Logo)
        Me.Controls.Add(Me.IdViviendaDato)
        Me.Controls.Add(Me.Volver)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Cerrar)
        Me.Controls.Add(Me.PuertaDato)
        Me.Controls.Add(Me.PlantaDato)
        Me.Controls.Add(Me.EdificioDato)
        Me.Controls.Add(Me.SectorDato)
        Me.Controls.Add(Me.PuertaLabel)
        Me.Controls.Add(Me.PlantaLabel)
        Me.Controls.Add(Me.EdificioLabel)
        Me.Controls.Add(Me.SectorLabel)
        Me.Controls.Add(Me.IdViviendaLabel)
        Me.Name = "Form3"
        Me.Text = "Form3"
        CType(Me.Logo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Logo As PictureBox
    Friend WithEvents IdViviendaDato As TextBox
    Friend WithEvents Volver As Button
    Friend WithEvents Button3 As Button
    Friend WithEvents Cerrar As Button
    Friend WithEvents PuertaDato As TextBox
    Friend WithEvents PlantaDato As TextBox
    Friend WithEvents EdificioDato As TextBox
    Friend WithEvents SectorDato As TextBox
    Friend WithEvents PuertaLabel As Label
    Friend WithEvents PlantaLabel As Label
    Friend WithEvents EdificioLabel As Label
    Friend WithEvents SectorLabel As Label
    Friend WithEvents IdViviendaLabel As Label
End Class
