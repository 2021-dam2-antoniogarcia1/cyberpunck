-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-12-2020 a las 16:53:26
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `vivienda`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vivienda`
--

CREATE TABLE `vivienda` (
  `IDVivienda` int(11) NOT NULL,
  `Sector` text NOT NULL,
  `Edificio` text NOT NULL,
  `Planta` int(11) NOT NULL,
  `Puerta` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `vivienda`
--

INSERT INTO `vivienda` (`IDVivienda`, `Sector`, `Edificio`, `Planta`, `Puerta`) VALUES
(1, '1', '12', 1, 3),
(2, '1', '34', 4, 1),
(3, '1', '23', 1, 1),
(4, '3', '45', 2, 1),
(5, '2', '76', 2, 1),
(6, '2', '13', 3, 3),
(7, '4', '56', 4, 2),
(8, '4', '63', 2, 3),
(9, '5', '24', 2, 2),
(10, '5', '05', 4, 1),
(11, '5', '94', 2, 3),
(12, '3', '65', 2, 4),
(13, '1', '35', 1, 3),
(14, '1', '26', 4, 1),
(15, '1', '26', 1, 1),
(16, '3', '26', 2, 1),
(17, '2', '56', 2, 1),
(18, '2', '27', 3, 3),
(19, '4', '26', 4, 2),
(20, '4', '28', 2, 3),
(21, '5', '19', 2, 2),
(22, '5', '26', 4, 1),
(23, '5', '24', 2, 3),
(24, '3', '46', 2, 4);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `vivienda`
--
ALTER TABLE `vivienda`
  ADD PRIMARY KEY (`IDVivienda`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `vivienda`
--
ALTER TABLE `vivienda`
  MODIFY `IDVivienda` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
