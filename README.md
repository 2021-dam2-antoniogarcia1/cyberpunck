# Cyberpunck

Con esta aplicación puedes acceder a la información de unas cuantas personas dentro de un mundo distócico y con temática cyberpunck.

## Contenidos:

- [Como funciona?](#Como-funciona?)


### Como funciona?

Al comenzar aparecerá un menú donde te pedirá que te loguees (esto es solamente decorativo, no afectara en nada). Una vez dentro aparecerán diferentes imágenes, si pulsas en una de ellas se te abrirá su información, donde podrás ver tanto sus estadísticas, como datos, donde vive, etc. Para cerrar simplemente hay que pulsar el botón de cerrar.
Este programa utiliza unas bases de datos ya creadas; para poder ser utilizadas hay que tener como usuario "root" y de contraseña "alumne", de lo contrario aparecerá un mensaje de error y no aparecerán los datos por pantalla.