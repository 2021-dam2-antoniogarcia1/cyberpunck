-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-12-2020 a las 16:53:42
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `estadisticas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estadísticas`
--

CREATE TABLE `estadísticas` (
  `Capacidades físicas` int(11) NOT NULL,
  `Capacidades intelectuales` int(11) NOT NULL,
  `Empatía` int(11) NOT NULL,
  `Ímpetu` int(11) NOT NULL,
  `Espiritualidad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `estadísticas`
--

INSERT INTO `estadísticas` (`Capacidades físicas`, `Capacidades intelectuales`, `Empatía`, `Ímpetu`, `Espiritualidad`) VALUES
(5, 5, 5, 5, 5),
(2, 3, 5, 3, 1),
(1, 3, 4, 2, 4),
(1, 1, 1, 1, 1),
(2, 3, 4, 1, 5),
(2, 4, 2, 5, 2),
(1, 1, 4, 2, 1),
(5, 4, 3, 1, 2),
(4, 3, 2, 4, 2),
(5, 5, 3, 5, 2),
(3, 4, 5, 2, 3),
(3, 5, 4, 3, 2),
(5, 5, 5, 5, 5),
(2, 3, 5, 3, 1),
(1, 3, 4, 2, 4),
(1, 1, 1, 1, 1),
(2, 3, 4, 1, 5),
(2, 4, 2, 5, 2),
(1, 1, 4, 2, 1),
(5, 4, 3, 1, 2),
(4, 3, 2, 4, 2),
(5, 5, 3, 5, 2),
(3, 4, 5, 2, 3),
(3, 5, 4, 3, 2);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
